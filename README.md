TODO: documentation, credits etc

This is a reversed copy of SetFSB 2.3.178.134 in an attempt to maintain it after it has been abandonware for 10 years.

Changes so far:

* Removed authentication check, leftover code still pending removal
* Removed datasave/crypt functions so that it will compile, need to check if any functionality is affected and bring them back

To compile you will need to load setfsbmod.hsp in the hsp351 compiler. I have included the 'original' base code as setfsb.ax/setfsb.hsp